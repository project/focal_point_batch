This module batch processes the Focal Point estimation on images that have not 
already been processed. If you have an entity with a large number of large 
images, you may get a white screen when trying to edit the entity because of 
the processing taking too long. This module will process each image file 
individually and allow you to edit the entity again.
